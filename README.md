# Curso de Docker - BMAT

En este repositorio encontrarás todos los recursos que se han visto en el curso. 

- [Documentación sobre Docker](docker.md)
- [Documentación sobre Dockerfile e imágenes](dockerfile.md)
- [Documentación sobre Docker Compose](docker-compose.md)

En cada directorio se encuentra el código para desplegar cada aplicación:
- [flask-app](flask-app): Ejemplo de webapp con Flask, con un Dockerfile optimizado y docker-compose
